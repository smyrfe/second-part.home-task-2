<?php
require('startup.php');
session_start();

//Функция, которая возвращает массив с существующими аккаунатами
/**
 * @param $path_to_json_files
 * @return array|mixed
 */
function get_list_accounts($path_to_json_files){
    $search_file = glob($path_to_json_files . "*.json"); //получаем список файлов с расширением .json
    $search_file = str_replace($path_to_json_files, "", $search_file);// удаляем директорию из строки
    $search_file = str_replace(".json", "", $search_file); //удаляем расширение
    return $search_file;
}
/*Поиск аккаунта, если есть такой пользовател возвращает - Valid,
если пароль неверный - Invalid Password,
если нет аккаунта - Account not found
*/
function  validationTest($userName, $password, $path_to_json_files)
{
    $search_file = get_list_accounts($path_to_json_files);
        if (in_array($userName,$search_file)) {
            $data = file_get_contents($path_to_json_files . $userName . ".json");
            $data_decode = json_decode($data, true);
            if (in_array($password, $data_decode, true)) {
                return "Valid";
            } else {
                return "Invalid password";
            }
        } else {
            return "Account not found";
        }
}





// Проверка роли пользователя
function getRole($userName, $path_to_json_files)
{
    $search_file = get_list_accounts($path_to_json_files);
    foreach ($search_file as $fileName) {
        if ($fileName === $userName) { //Если есть файл с именем, совпадающим с  аргументом $userName, то есть такой пользователь и идет проверка его роли
            $data = file_get_contents($path_to_json_files . $fileName . ".json");
            $data_decode = json_decode($data, TRUE);
            foreach ($data_decode as $key => $Role) {
                if ($key === "Role") {
                    return $Role;
                }
            }
        }
    }
}
//Вывод списка зарегистрированых пользователей
function view_all_members($path_to_json_files){

    $search_file = get_list_accounts($path_to_json_files);
    foreach ($search_file as $key => $fileName) {  ?>
        <form method="POST" action="about_user.php">
        <table>
            <tr>
                <td>Имя пользователя</td>
                <td>Роль</td>
                <td></td>
            </tr>
            <tr>
                <td><?php echo $fileName;?></td>
                <td><?php echo getRole("$fileName","$path_to_json_files");?></td>
                <td><button type="submit" name="user" value="<?php echo $fileName?>">О пользователе</button></td>
            </tr>
        </table>
        </form>
   <?php }

} ?>
<?php //Вывод информации о $userName пользователе
function about_user($userName, $path_to_json_files)
{
    $search_file = get_list_accounts($path_to_json_files);
    foreach ($search_file as $fileName) {
        if ($fileName === $userName) {
            $data = file_get_contents($path_to_json_files . $fileName . ".json"); //открываем json файл
            $data_decode = json_decode($data, TRUE); //декодируем его
            foreach ($data_decode as $key => $value) {
                if ($key != "Password"){
                    echo "<h3>" . $key . ":" . " " . $value . "</h3>";
                }
            }
        }
    }
}
function check_uniqueness_user($userName, $path_to_json_files){
    $search_file = get_list_accounts($path_to_json_files);
    if(in_array($userName, $search_file)){
        return false;
    } else {
        return true;
    }
}
