<?php
require "startup.php";
require "functions.php";

$_SESSION['Role'] = getRole("$_SESSION[userName]","../../userdata/json/accounts/");
 if (isset($_SESSION['userName'])) {
     echo "<h2>" . 'Здравствуйте, ' . $_SESSION['userName'] . "," . " " . "ваша роль -" . " " . $_SESSION['Role'] . "</h2>";
 } else {
     header('Location: ../index.php');
    }
?>
<?php //Если текущ. пользователь админ, то выводим список пользователей в системе
if ($_SESSION['Role'] === "Admin" ){ ?>
    <h3>Список пользователей в системе</h3>
    <?php view_all_members("../../userdata/json/accounts/");
}
?>
<title>Добро пожаловать!</title>
<link  rel="stylesheet" type="text/css"  href="../css/align_inputs.css">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu sapien vitae nisi eleifend convallis quis sit amet tellus.
    Fusce imperdiet, metus ac rutrum scelerisque, dolor
    ligula posuere sapien, ut iaculis urna odio non sem. Ut semper maximus nulla, eget ultricies purus
    fringilla quis. Maecenas nisl nisi, tincidunt id purus ac,
    convallis sollicitudin lacus. Phasellus et tincidunt tellus. Etiam mollis dolor nibh,
    quis mollis mi convallis quis. Praesent non leo rhoncus, vestibulum ante ut, sodales velit.</p>
<form method="POST" action="handlers/handler.php">
    <?php if(getRole("$_SESSION[userName]","../../userdata/json/accounts") === "Admin"){?>
        <button type="submit" name="addNewUser">Добавить нового пользователя</button>
    <?php }?>
    <button type="submit" name="changeInfo">Изменить профиль</button>
    <button type="submit" name="logout">Выход</button>

</form>


