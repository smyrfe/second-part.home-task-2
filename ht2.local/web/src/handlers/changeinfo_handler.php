<?php
include '../startup.php';
include 'authentication_handler.php';
 if (isset($_POST['newFirstName']) && isset($_POST['newLastName']) && isset($_POST['newCity'])
     && isset($_POST['newEmail']) && isset($_POST['newPassword']) && isset($_POST['Sex'])) {

     $_SESSION['newUserName'] = strtolower("$_POST[newEmail]"); //переводим в ниж.регистр новое имя пользователя
     $dir = "../../json/accounts/"; //Директория в которой хранятся Json файлы
     $oldPath = $dir . "$_SESSION[userName]" . ".json"; //Путь к файлу с текущим именем пользователя
     $newPath = $dir . "$_SESSION[newUserName]" . ".json"; //Путь к файлу с изменненым именем пользователя
     //Массив с обновленными данными о пользователе
     $newInformation = array(
         'FirstName' => $_POST['newFirstName'],
         'LastName' => $_POST['newLastName'],
         'City' => $_POST['newCity'],
         'Email' => $_POST['newEmail'],
         'Password' => $_POST['newPassword'],
         'Sex' => $_POST['Sex'],
     );
     $search_file = glob("$dir" . "*.json");
     $search_file = str_replace($dir, "", $search_file);// удаляем "../json/accounts/" из строки
     $search_file = str_replace(".json", "", $search_file); //удаляем ".json"

     foreach ($search_file as $fileName) {
         if ($fileName == $_SESSION['userName']) {
             $data = file_get_contents($dir . $fileName . ".json"); //открываем файл с json строкой
             $data_decode = json_decode($data, true);
             $data_decode = array_replace($data_decode, $newInformation);
             file_put_contents($oldPath, json_encode($data_decode));
             rename($oldPath, $newPath); //переименовываем имя файла в соотв. с новым именем пользователя
             $_SESSION['userName'] = $_SESSION['newUserName'];
             header("Location: ../login.php");
         }
     }
 }
 if (isset($_POST['back'])){
     header ("Location: ../login.php");
 }
