<?php
require "../functions.php";
//Если нажата кнопка зарегистрироваться
    if (isset($_POST['addUser'])) {
        //Проверяем заполнены ли все поля
        if (strlen($_POST['Email']) > 0 && strlen($_POST['Password']) > 0) {
            $loginName = mb_strtolower($_POST['Email']); //Переводим строку с  логином в ниж. регистр
            $_SESSION['unique'] = check_uniqueness_user($loginName, "../../../userdata/json/accounts/"); //Проверяем на уникальность Email
            if ($_SESSION['unique']) {
                $path = "../../../userdata/json/accounts/" . $loginName . ".json"; //Путь к папке с аккаунтами
                /*Проверяем авторизировался ли пользователь, если да и он является админом, то по ключу 'Role'
                * будет передаваться выбранное им значение, а если нет, то по умолчанию присвается Member
                */
                if (isset($_SESSION['userName']) && $_SESSION['Role'] === "Admin") {
                    $newMember = array(
                        'FirstName' => $_POST['FirstName'],
                        'LastName'  => $_POST['LastName'],
                        'City'      => $_POST['City'],
                        'Email'     => $_POST['Email'],
                        'Password'  => $_POST['Password'],
                        'Sex'       => $_POST['Sex'],
                        'Role'      => $_POST['Role']
                    );
                    file_put_contents($path, json_encode($newMember));
                    header("Location: ../../index.php");
                } else {
                    $newMember = array(
                        'FirstName' => $_POST['FirstName'],
                        'LastName'  => $_POST['LastName'],
                        'City'      => $_POST['City'],
                        'Email'     => $_POST['Email'],
                        'Password'  => $_POST['Password'],
                        'Sex'       => $_POST['Sex'],
                        'Role'      => "Member"
                    );
                    file_put_contents($path, json_encode($newMember));
                    header("Location: ../../index.php");
                }
            } else {
                header("Location: ../error_registration.php");
            }
        } else {
            $_SESSION['emptyValues'] = "Empty values";
            header("Location: ../error_registration.php");
        }
    } elseif (isset($_POST['mainPage'])) {
        header("Location: ../../index.php");
    }