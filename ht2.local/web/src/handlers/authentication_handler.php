<?php
require "../functions.php";
require "../startup.php";
if (isset($_POST['login'])) {
    if (strlen($_POST['userName']) > 0 && strlen($_POST['Password']) > 0) {
        if (validationTest("$_POST[userName]", "$_POST[Password]", "../../../userdata/json/accounts/") === "Valid") {
            $_SESSION['userName'] = $_POST['userName'];
            $_SESSION['Password'] = $_POST['Password'];
            header("Location: ../login.php");
        } elseif (validationTest("$_POST[userName]", "$_POST[Password]", "../../../userdata/json/accounts/") === "Invalid password") {
            $_SESSION['userName'] = $_POST['userName'];
            $_SESSION['invalidPassword'] = "Invalid password";
            header("Location: ../error_auth.php");
        } elseif (validationTest("$_POST[userName]", "$_POST[Password]", "../../../userdata/json/accounts/") === "Account not found") {
            $_SESSION['userName'] = $_POST['userName'];
            $_SESSION['accountNotFound'] = "Account not found";
            header("Location: ../error_auth.php");
        }
    } else{
        $_SESSION['emptyValues'] = "Empty values";
        header("Location: ../error_auth.php");
    }
}
 elseif (isset($_POST['registration'])) {
    header("Location: ../registration.php");
}
