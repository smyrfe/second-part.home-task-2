<?php

if (isset($_POST['login'])){
    header ("Location: authentication_handler.php");
} elseif (isset($_POST['registration'])){
    header ("Location: ../registration.php");
} elseif (isset($_POST['changeInfo'])){
    header ("Location: ../change_info.php");
} elseif (isset($_POST['logout'])) {
    header("Location: ../destroy_session.php");
} elseif (isset($_POST['addNewUser'])){
    header("Location: ../registration.php");
} elseif (isset($_POST['mainPage'])){
    header("Location: ../../index.php");
}