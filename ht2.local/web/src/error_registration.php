<?php
session_start();
if (isset($_SESSION['unique']) && !$_SESSION['unique']) {
    echo "<h2>" . "Ошибка при регистрации!" . " " . "Пользователь с таким именем уже существует" . "</h2>";
    session_destroy();
} elseif (isset($_SESSION['emptyValues']) && $_SESSION['emptyValues'] == "Empty values"){
    echo "<h2>" . "Ошибка при регистрации!" . " " . "Вы не заполнили все поля для ввода" . "</h2>";
    session_destroy();
}
?>
<form method="POST" action="handlers/handler.php">
    <button type="submit" name="registration" >Вернуться на страницу регистрации</button>
    <button type="submit" name="mainPage">На главную</button>
</form>
