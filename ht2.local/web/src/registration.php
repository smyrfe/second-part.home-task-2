<?php
require "functions.php";
require("handlers/handler.php");
?>
<title>Регистрация нового пользователя</title>
<link  rel="stylesheet" type="text/css"  href="../css/align_inputs.css">
<h1>Регистрация нового пользователя</h1>
<form method="POST" action="handlers/reg_handler.php">
    <div class="main">
    <div class="field">
        <label>Имя:</label>
        <input type="text" name="FirstName"><br>
    </div>
    <div class="field">
        <br><label>Фамилия:</label>
        <input type="text" name="LastName"><br>
    </div>
    <div class="field">
        <br><label>Город:</label>
        <input type="text" name="City"><br>
    </div>
    <div class="field">
        <br><label>E-mail:</label>
        <input type="text" name="Email"><br>
    </div>
    <div class="field">
        <br><label>Пароль:</label>
        <input type="password" name="Password"><br>
    </div>
    <div class="filed">
        <?php if (isset($_SESSION['userName'])){
            if(getRole("$_SESSION[userName]","../../userdata/json/accounts") === "Admin"){ ?>
                <br><label>Роль пользователя</label>
                <br><select name="Role">
                    <option value="Admin">Администратор</option>
                    <option value="Member">Участник</option>
                </select><br>
            <?php }
        } ?>
    </div>
    <div class="field">
        <br><label>Пол:</label>
        <select id="select" name="Sex">
            <option value="Man">Мужской</option>
            <option value="Woman">Женский</option>
        </select><br>
    </div>
        <br><button type="submit" name="addUser">Зарегистрироваться</button>
        <button type="submit" name="mainPage">На главную</button>
    </div>

</form>



